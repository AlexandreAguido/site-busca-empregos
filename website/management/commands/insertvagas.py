import os
import json
from django.core.management.base import BaseCommand
from django.db.utils import IntegrityError
from django.conf import settings
from google_drive import Drive
from geonames import Geonames
from website.models import * 

class Command(BaseCommand):
    help = 'download files with job posts and insert into the database'
    def __init__(self, *args, **kwargs):
        super(Command, self).__init__(*args, **kwargs)
        self.geonames = Geonames()

    def parse_file(self, bla, file_path):
        vagas = json.load(open(file_path))
        has_estado = 'uf' in vagas[0].keys()
        for vaga in vagas:
            if has_estado:
                uf = vaga['uf']
            else:
                uf = None
            cidade = self.get_or_create_cidade(vaga['cidade'], uf)
            empresa = Empresa.objects.get_or_create(vaga['empresa'])[0]
            try:
                v = Vaga(
                    url = vaga['url'],
                    titulo = vaga['titulo'],
                    descricao = vaga['descricao'],
                    empresa = empresa,
                    salario = vaga['salario'],
                    estado = cidade.estado,
                    cidade = cidade
                )
            except:
                print(cidade)
            v.quantidade = vaga.get('quantidade', 1)
            v.pdc = vaga.get('pdc', False)
            try:
                v.save()
            except Exception as e:
                if type(e) != IntegrityError:
                    raise(e)


    def handle(self, *args, **kwargs):
        self.stdout.write('Creating Instance of object')
        drive = Drive()
        self.stdout.write('Downloading Files')
        drive.get_all_files(settings.WEBSITE_STORAGE)
        files = filter(lambda x : x.endswith('json'), os.listdir(settings.WEBSITE_STORAGE))
        for file in files:
            self.stdout.write('Parsing File: ' + file)
            path = settings.WEBSITE_STORAGE + file
            self.parse_file(self, path)
    
    def get_or_create_cidade(self, nome_cidade, uf):
        estado = None
        if uf:
            estados = Estado.objects.filter(uf=uf) 
            if len(estados): estado = estados[0]
        cidade = Cidade.objects.filter(nome=nome_cidade)
        if not len(cidade):
            estado = self.geonames.get_state_name_from_city(nome_cidade)
            uf = self.geonames.get_uf_from_city(nome_cidade)
            try: estado = Estado.objects.get_or_create(nome=estado, uf=uf)[0]
            except Exception as e:
                print(*Estado.objects.all(), sep="\n")
                raise(e)
            cidade = Cidade.objects.get_or_create(nome=nome_cidade, estado=estado)[0]
        # filtar por estado
        elif uf and estado:
            cidade = Cidade.objects.get_or_create(nome=nome_cidade, estado=estado)[0]
        else:
            cidade = cidade[0]
        return cidade

    
