from django.db import models
 

class Estado(models.Model):
    nome = models.CharField(max_length=50, unique=True)
    uf = models.CharField(max_length = 2, unique=True, blank=False)

    def __str__(self):
        return f'{self.nome}/{self.uf}'

class Cidade(models.Model):
    nome = models.CharField(max_length=40)
    estado = models.ForeignKey(Estado, models.CASCADE)

class Empresa(models.Model):
    nome = models.CharField(max_length=50)
    email = models.CharField(max_length=40)

class Vaga(models.Model):
    url = models.CharField(max_length=128, unique=True)
    titulo = models.CharField(max_length=60)
    descricao = models.TextField()
    salario = models.FloatField(default=0)
    quantidade = models.IntegerField(default=1)
    pdc = models.BooleanField(default=False)
    criado_em = models.DateTimeField(auto_now=True)
    estado = models.ForeignKey(Estado, models.CASCADE)
    cidade = models.ForeignKey(Cidade, models.CASCADE)
    empresa = models.ForeignKey(Empresa, models.CASCADE, null=True)