from django.urls import path, include, re_path
from . import views

# app_name='lists'
urlpatterns = [
    path('vaga/<titulo>/<int:_id>/', views.get_vaga, name='vaga'),
    path('vagas', views.get_vagas, name="vagas"),
    re_path('vagas/(?P<page_number>\d*)', views.get_vagas, name='vagas'),
    path('', views.index, name='home'),
]