import os
from django.test import TestCase
from geonames import Geonames
from .models import *


class GeonamesTest(TestCase):
    def setUp(self):
        if not os.environ.get('GEONAMES_KEY'):
            raise Exception('GEONAMES_KEY not defined')
        self.geonames = Geonames()

    def test_can_get_correct_state_from_city(self):
        self.assertEqual('São Paulo',
                         self.geonames.get_state_name_from_city('santos'))
        self.assertEqual('São Paulo',
                         self.geonames.get_state_name_from_city('campinas'))
        self.assertEqual('Sergipe',
                         self.geonames.get_state_name_from_city('aracaju'))
        self.assertEqual('São Paulo',
                         self.geonames.get_state_name_from_city('Atibaia'))

    def test_can_get_correct_uf_from_city(self):
        self.assertEqual('SE',
                         self.geonames.get_uf_from_city('Estância'))
        self.assertEqual('BA',
                         self.geonames.get_uf_from_city('Salvador'))


class EmpresaTest(TestCase):
    def test_can_create_empresa(self):
        empresa = Empresa.objects.get_or_create(nome='empresa1')
        self.assertEqual(Empresa.objects.count(), 1)


class HomePageTest(TestCase):
    def test_index_render_correct_template(self):
        resp = self.client.get('')
        self.assertTemplateUsed(resp, 'website/index.html')


class VagasTest(TestCase):
    def setUp(self):
        self.estado = Estado.objects.create(nome='Bahia', uf='BA')
        self.cidade = Cidade.objects.create(
            nome='Salvador', estado=self.estado)
        self.vaga_obj = {
            'url': 'www.test.com/123123123',
            'titulo': 'vaga teste',
            'descricao': 'vaga teste',
            'salario': 1,
            'estado': self.estado,
            'cidade': self.cidade,
        }
        self.vaga = Vaga.objects.create(**self.vaga_obj)

    def test_return_correct_vaga(self):
        resp = self.client.get(f'/vaga/{self.vaga.titulo}/{self.vaga.id}/')
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(self.vaga, resp.context['vaga'])

    def test_redirect_if_vaga_not_found(self):
        resp = self.client.get(f'/vaga/{self.vaga.titulo}/0/')
        self.assertRedirects(resp, '/vagas')
