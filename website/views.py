from django.shortcuts import render, redirect
from django.core.paginator import Paginator
from django.http import HttpResponse
import json
from .models import Vaga
from random import choice

def index(request):
    vagas = Vaga.objects.order_by('-id')[:15]
    if vagas:
        vaga = choice(vagas)
    else:
        vaga = []
    return render(request, 'website/index.html', {
        'vaga': vaga,
        'vagas': vagas
    })

def get_vaga(request, titulo, _id):
    titulo = titulo.replace('_', ' ')
    try:
        vaga = Vaga.objects.get(titulo=titulo, id=_id)
    except Exception as e:
        return redirect('/vagas')
    return render(request, 'website/vaga.html', {'vaga': vaga})

def get_vagas(request, page_number=1):
    vagas = Vaga.objects.order_by('-id')
    page = Paginator(vagas, 15).get_page(page_number)
    previous_page = 0 if not page.has_previous() else page.previous_page_number()
    next_page = 0 if not page.has_next() else page.next_page_number()
    context = {
        'page_data': page,
        'next_page': next_page,
        'previous_page': previous_page
    }
    return render(request, 'website/vagas.html', context)
