from django import template

register = template.Library()

@register.filter()
def snake_case(value):
    return value.replace(' ', '_')