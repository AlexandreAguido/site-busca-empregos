import locale
from django import template

register = template.Library()

@register.filter()
def snake_case(value):
    return value.replace(' ', '_')

@register.filter()
def to_real(value):
    if type(value) not in (int, float) or value < 1:
        return False
    return locale.currency(value, grouping=True)

@register.simple_tag()
def format_local(cidade, uf):
    if not cidade and uf:
        return ''
    return f"{cidade}, {uf}"