import os
import requests
from django.conf import settings

QUERY_ENDPOINT = 'http://api.geonames.org/search?'

class Geonames:
    def __init__(self):
        self.last_city = ''
        self.last_search = ''

    def _search(self, query):
        url = QUERY_ENDPOINT + 'username={}&q={}&maxRows={}&country=br&featureClass=P&type=json'
        url = url.format(settings.GEONAMES_KEY, query, 5)
        resp = requests.get(url).json()
        return resp
    
    def get_state_name_from_city(self, city):
        data = self._search(city)
        try:
            state = data['geonames'][0]['adminName1'];
        except Exception as e:
            print(data)
            raise(e)
        self.last_city = city.lower()
        self.last_search = data
        return state

    def get_uf_from_city(self, city):
        if self.last_city == city.lower():
            data = self.last_search
        else:
            data = self._search(city)
        try:
            uf = data['geonames'][0]['adminCodes1']['ISO3166_2'];
        except Exception as e:
            raise(e)
        return uf        
    
